\documentclass{labo}

\usepackage[utf8]{inputenc}

\lezione{4}{17 Novembre 2010}

\begin{document}

\maketitle

\disclaimer

\begin{esercizio}[di riscaldamento]
Scrivere una funzione \lstinline{function M=laplacian(n)} che crea la matrice di dimensione $n \times n$ che ha $2$ sulla diagonale e $-1$ sulla sopra- e sotto-diagonale:
\begin{lstlisting}
octave:1> laplacian(5)
ans =

   2  -1   0   0   0
  -1   2  -1   0   0
   0  -1   2  -1   0
   0   0  -1   2  -1
   0   0   0  -1   2

\end{lstlisting}

\end{esercizio}

\section{Soluzione di sistemi triangolari}

\subsection{Sistemi triangolari inferiori}
\[
\begin{pmatrix}
 L_{11} & 0 & 0 & 0 & 0\\
 L_{21} & L_{22} & 0 & 0 & 0\\
 L_{31} & L_{32} & L_{33} & 0 & 0\\
 L_{41} & L_{42} & L_{43} & L_{44} & 0\\
 L_{51} & L_{52} & L_{53} & L_{54} & L_{55}
\end{pmatrix}
\begin{pmatrix}
 x_1\\x_2\\x_3\\x_4\\x_5
\end{pmatrix}
=
\begin{pmatrix}
 b_1\\b_2\\b_3\\b_4\\b_5
\end{pmatrix}
\]

Possiamo risolverlo per sostituzione: a ogni passo, supponendo di avere calcolato $x_1,\dots,x_{i-1}$ si ha
\[
 x_i=\frac{b_i-\sum_{j=0}^{i-1} L_{ij}x_j }{L_{ii}}
\]

\begin{lstlisting}
function x=inf_solve(L,b)
  n=size(L)(1);
  x=zeros(n,1);
  for i=1:n
    p=b(i); % accumulatore    
    for j=1:i-1
      p=p-L(i,j)*x(j);
    endfor
    x(i)=p/L(i,i);
  endfor
endfunction
\end{lstlisting}

Testiamo la funzione con la matrice \lstinline{L=tril(laplacian(5))} e il termine noto \lstinline{L*y}, dove $y$ è un vettore semplice.

\begin{lstlisting}
octave:4> y=[1:5]'
y =

  1
  2
  3
  4
  5

octave:5> y=[1:5]'
y =

  1
  2
  3
  4
  5

octave:6> L=tril(laplacian(5))
L =

   2   0   0   0   0
  -1   2   0   0   0
   0  -1   2   0   0
   0   0  -1   2   0
   0   0   0  -1   2

octave:7> inf_solve(L,L*y)
ans =

  1
  2
  3
  4
  5
\end{lstlisting}

\begin{esercizio}
Scrivere una \lstinline{function sup_solve(U,b)} che risolva un sistema $Ux=b$ con $U$ triangolare superiore (hint: sostituire a partire dall'ultima riga). Testare su \lstinline{U=triu(laplacian(5))}, \lstinline{b=U*y} (con \lstinline{y} vettore opportuno).
\end{esercizio}

\subsection{Soluzione di sistemi con la matrice di Laplace}

Il comando \lstinline{[L,U]=lu(laplacian(5))} restituisce due matrici, una $L$ triangolare inferiore, e una $U$ triangolare superiore, tali che \lstinline{L*U=laplacian(5)}.
\begin{lstlisting}
octave:9> [L,U]=lu(laplacian(5))
L =

   1.00000   0.00000   0.00000   0.00000   0.00000
  -0.50000   1.00000   0.00000   0.00000   0.00000
   0.00000  -0.66667   1.00000   0.00000   0.00000
   0.00000   0.00000  -0.75000   1.00000   0.00000
   0.00000   0.00000   0.00000  -0.80000   1.00000

U =

   2.00000  -1.00000   0.00000   0.00000   0.00000
   0.00000   1.50000  -1.00000   0.00000   0.00000
   0.00000   0.00000   1.33333  -1.00000   0.00000
   0.00000   0.00000   0.00000   1.25000  -1.00000
   0.00000   0.00000   0.00000   0.00000   1.20000

octave:10> L*U-laplacian(5)
ans =

  0  0  0  0  0
  0  0  0  0  0
  0  0  0  0  0
  0  0  0  0  0
  0  0  0  0  0

\end{lstlisting}

Utilizzando questa fattorizzazione e le funzioni \lstinline{inf_solve} e \lstinline{sup_solve} possiamo scrivere una funzione che risolve un sistema $Ax=b$ con \lstinline{A=laplacian(5)}: infatti,
\[
 A^{-1}b = (LU)^{-1}b = U^{-1}(L^{-1}b)
\]
\begin{lstlisting}
function x=lap_solve(n,b)
  [L,U]=lu(laplacian(n));
  y=inf_solve(L,b)
  x=sup_solve(U,y);
endfunction
\end{lstlisting}
Testiamo:
\begin{lstlisting}
octave:20> A=laplacian(5); y=[1:5]';
octave:21> lap_solve(5,A*y)
ans =

  1.0000
  2.0000
  3.0000
  4.0000
  5.0000
\end{lstlisting}
Possiamo testarlo anche su matrici molto più grandi: per $n=1000$ sui computer del laboratorio dovrebbe impiegare meno di 20 secondi.

\begin{treat}
 Qual è l'errore relativo (in norma-2) ottenuto sulla soluzione per diversi valori di $n$? Come è legato al condizionamento della matrice \lstinline{laplacian(n)}, che potete calcolare usando il comando \lstinline{cond} di Octave?
\end{treat}

\subsection{Un problema fisico}
Vogliamo determinare qual è la forma assunta da una corda (o da un elastico, o da un ponte) su cui sono appoggiati dei carichi. Modellizziamo la corda come una serie di ``particelle'' $P_0,P_1,P_2,\dots,P_{n},P_{n+1}$ disposte a intervalli regolari, a ognuna delle quali è applicato un certo peso. Supponiamo che $x_0,y_0$ e $x_{n+1},y_{n+1}$ siano fissati. Supponiamo inoltre che le particelle possano muoversi solo in verticale, cioè che le $x_i$ siano fissate a intervalli regolari.

\begin{center}
 \includegraphics{ponte}
\end{center}

Su ogni punto $P_i$ \emph{interno} alla corda agiscono tre forze:
\begin{itemize}
 \item il carico $c_i$ (verso il basso) che indica quanto peso viene appoggiato sul punto $P_i$
 \item la forza $F_{i,i+1}$ che ``tiene incollato'' il punto al punto alla sua destra; la sua componente verticale è circa uguale a $k(y_{i+1}-y_i)$ (per un'opportuna costante elastica $k$)
 \item la forza $F_{i,i-1}$ che ``tiene incollato'' il punto al punto alla sua sinistra; la sua componente verticale è circa uguale a $k(y_{i-1}-y_{i})$.
\end{itemize}
L'equilibrio si ha quando $c_i=k(y_{i+1}-y_i)+k(y_{i-1}-y_{i})$, o anche
\[
 -y_{i-1}+2y_i-y_{i+1}=-c_i/k.
\]
Possiamo scrivere queste equazioni come un sistema lineare $Ly=b$, dove $L$ è la matrice di Laplace vista sopra, $y=(y_1,y_2,\dots,y_n)$ sono le altezze dei punti interni al ponte, e
\[
 b=\begin{bmatrix}
    y_0-c_1\\
-c_2\\
-c_3\\
\vdots\\
-c_{n-1}\\
y_{n+1}-c_n
   \end{bmatrix}
\]
(come mai il primo e l'ultimo termine sono diversi?). Possiamo porre $k=1$ (tanto dipende solo dalle unità di misura).

\begin{esercizio}
Ponete $n=20$. Scrivete il termine noto $b$ per i carichi $c_i=1$ per ogni $i$, $y_0=y_{n+1}=0$. Risolvete il sistema per trovare le altezze dei punti, e disegnate il ``ponte'' con il comando \lstinline{plot(1:20,y)}.
\end{esercizio}
\begin{esercizio}
 Provate a cambiare i carichi aggiungendo dei pesi in punti specifici del ponte. Provate a cambiare le altezze dei due estremi del ``ponte''.
\end{esercizio}
\begin{center}
 \includegraphics{plot}
\end{center}

\begin{treat}
Calcolate risolvendo un sistema simile anche le ascisse $x$ dei punti. Qual è il risultato? Come ve lo spiegate?
\end{treat}

\section{Facoltativo: sfruttare la struttura}
\subsection{Matrici bidiagonali}
Notiamo che le matrici $L$ e $U$ per le matrici \lstinline{laplacian(n)} hanno una struttura particolare: tutti gli elementi sono nulli tranne quelli sulla diagonale principale e sulla sottodiagonale (per $L$) o sopradiagonale (per $U$). $L$ e $U$ sono dette \emph{bidiagonali}. Possiamo sfruttare esplicitamente questo fatto nei nostri algoritmi.
\begin{treat}
 Scrivere due funzioni \lstinline{inf_solve2} e \lstinline{sup_solve2} che risolvano i sistemi $Ly=b$ e $Ux=y$ supponendo $L$ e $U$ bidiagonali (facendo meno calcoli di \lstinline{inf_solve} e \lstinline{supsolve}!). Poi scrivere la funzione \lstinline{lap_solve2} analoga a \lstinline{lap_solve} ma che utilizza le due funzioni appena scritte.
\end{treat}

Quanto tempo impiegano le due funzioni?
\begin{lstlisting}
octave:27> b=laplacian(1000)*[1:1000]';
octave:28> tic;lap_solve(1000,b); toc
ans = 11.669
octave:29> tic;lap_solve2(1000,b); toc
ans = 2.1535
\end{lstlisting}

\paragraph{Morale della favola:} quando una matrice ha una struttura dobbiamo sempre cercare di sfruttarla nei calcoli; il guadagno di tempo (e a volte anche di precisione dei risultati) può essere notevole.
\end{document}
