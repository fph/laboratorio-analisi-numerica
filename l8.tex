\documentclass{labo}

% rubber: pdf

\usepackage[utf8]{inputenc}

\lezione{8}{19 gennaio 2011}

 \begin{document}

\maketitle

\disclaimer

\section{Frattali di Newton}
In questa lezione cercheremo di disegnare i frattali che si ottengono disegnando i bacini di attrazione del metodo di Newton (sul piano complesso) per un polinomio. Le immagini risultanti dovrebbero assomigliare a quella in figura~\ref{fignewton}.
\begin{figure}[h!bt]\label{fignewton}
\begin{center}
% \includegraphics[width=.7\textwidth]{newtonfractal480.png}
\includegraphics[width=.7\textwidth]{newton.png}
\end{center}
\caption{Disegno dei bacini di attrazione del metodo di Newton per il polinomio $x^3+1$. I tre colori diversi corrispondono ai punti del piano complesso a partire da cui Newton converge alle tre diverse radici.}
\end{figure}
Notate la simmetria della figura: a seconda del quadrante del piano complesso da cui partiamo, si ha convergenza alla più vicina delle tre radici; però, nei punti che sono circa equidistanti da due delle tre radici, si ha un comportamento caotico.

Cominciamo dividendo il problema in molti sottoproblemi più semplici.
\subsection{Manipolazione di polinomi}
Dato un polinomio, lo rappresentiamo come il vettore dei suoi coefficienti: ad esempio, a $x^3-1$ corrisponde il vettore \lstinline{[1;0;0;-1]}. Notare che se il polinomio ha grado $n$, il vettore ha lunghezza $n+1$.

Il metodo di Horner per valutare un polinomio corrisponde a fare i prodotti associandoli in questo modo: per esempio, per un polinomio di grado 4,
\[
 a(x)=(((a_4*x+a_3)*x+a_2)*x+a_1)*x+a_0.
\]
\begin{esercizio}
 Scrivere una \lstinline{function y=horner(p,x)} che prende un polinomio $p$ (rappresentato come il vettore dei suoi coefficienti) e un numero $x$ e calcola $p(x)$ con il metodo di Horner.
\end{esercizio}

\begin{esercizio}
 Scrivere una \lstinline{function dp=derivata(p)} che prende un polinomio $p$ (vettore di coefficienti) e restituisce la sua derivata (vettore di coefficienti).
\end{esercizio}

\subsection{Metodo di Newton}
Il metodo di Newton è l'iterazione
\[
 x_{k+1}=x_k-\frac{p(x_k)}{p'(x(k)}.
\]
\begin{esercizio}
 Scrivere una \lstinline{function x=newton(p,x0)} che esegue il metodo di Newton sul polinomio $p$ partendo dal punto iniziale \lstinline{x0}. Come criterio di arresto, si può usare quello di terminare se $|p(x)| \leq 10^{-12}$:
\begin{lstlisting}
function x=newton(p,x0);
  x=x0;
  px=horner(p,x0);
  while(abs(px)>1E-12)
    %calcola il nuovo x e il nuovo px
  endwhile
endfunction
\end{lstlisting}
\end{esercizio}

Testare il metodo di Newton sul polinomio $p(x)=x^3+1$. Quali sono le sue radici? Riuscite a trovare un valore iniziale per il metodo di Newton che lo faccia convergere ad ognuna di esse? Ricordate che il modo più semplice per inserire un numero complesso in Octave è \lstinline{2+3I}.

\subsection{``Disegnare'' una matrice}
La funzione \lstinline{imagesc} prende come parametro una matrice $m \times n$ $A$, e genera un'immagine $m \times n$ in cui il pixel $i,j$ è colorato di un colore che varia su una scala da rosso a blu a seconda di quanto il valore di $A_{i,j}$ è grande/piccolo rispetto agli altri elementi della matrice. Per esempio, con
\begin{lstlisting}
 octave:1> imagesc(eye(100))
\end{lstlisting}
viene visualizzata un'immagine in cui la diagonale (elementi più grandi) è rossa, e tutti gli altri elementi (elementi più piccoli) sono blu. Provate anche
\lstinline{imagesc(laplacian(10))} o \lstinline{imagesc(rand(100))}.
\begin{figure}
\begin{center}
 \includegraphics[width=0.45\textwidth]{img1}
\includegraphics[width=0.45\textwidth]{img2}
\end{center}
\caption{\lstinline{imagesc(eye(100))} e \lstinline{imagesc(laplacian(10))}}
\end{figure}
\subsection{Frattale di Newton}
Per disegnare il frattale di Newton relativo al polinomio $p(x)=x^3+1$, abbiamo bisogno innanzitutto di una funzione che ``decida'' a quale valore c'è convergenza.
\begin{esercizio}
Scrivere una funzione \lstinline{function val=decidi(x)} che restituisca $1$, $2$ o $3$ a seconda se il numero complesso $x$ è più vicino a $-1$, a $\frac 12 +I\frac{\sqrt{3}}{2}$, o a $\frac 12 -I\frac{\sqrt{3}}{2}$.
\end{esercizio}

\begin{esercizio}
 Scrivere una \lstinline{function img=newtonfractal()} che non prende alcun argomento e restituisce una matrice $101 \times 101$ chiamata \lstinline{img} calcolata in questo modo:
\begin{itemize}
 \item genera 101 valori equispaziati nell'intervallo $[-2,2]$ con l'istruzione \lstinline{range=-2:0.04:2}.
 \item per ogni coppia $(i,j)$:
\begin{itemize}
\item calcola il punto $z_0$ del piano complesso \lstinline{z0=range(i)+1I*range(j)};
\item esegue il metodo di Newton per il polinomio $x^3+1=0$ partendo dal punto \lstinline{z0};
\item utilizzando la funzione \lstinline{decidi()}, scrive $1$, $2$ o $3$ in \lstinline{img(i,j)} a seconda della radice del polinomio a cui si ha convergenza a partire dal valore iniziale \lstinline{z0}.\footnote{potete dare per scontato che il metodo di Newton converga per tutti i valori iniziali nel nostro range.}.
\end{itemize}
 \item restituisce la matrice \lstinline{img}
\end{itemize}
\end{esercizio}
La funzione qui scritta sarà probabilmente abbastanza lenta (potrebbe metterci un mezzo minuto\dots) e restituirà una matrice \lstinline{img} che potrete poi visualizzare a schermo con l'istruzione \lstinline{imagesc(img)}. Assomiglia alla figura~\ref{fignewton}?


\section{Esercizi facoltativi}
\begin{treat}
Generate l'immagine corrispondente per il metodo di Newton su altri polinomi. Non sapete le radici? Potete farle calcolare a Octave: la funzione \lstinline{roots(p)} calcola le radici di un polinomio (rappresentato come vettore di coefficienti): per esempio,
\begin{lstlisting}
 octave:2> roots([1 -1 -1 ])   %radici di x^2-x-1=0
ans =

  -0.61803
   1.61803
\end{lstlisting}
Se volete, potete riscrivere le funzioni scritte finora in modo che il polinomio \lstinline{p} non sia fissato ma sia uno degli argomenti.
\end{treat}

\subsection{Frattale di Julia}
Il \emph{frattale di Julia} relativo al numero complesso $c$ è definito come l'insieme dei punti $z_0$ per cui la successione definita da $z_{k+1}=z_k^2+c$ non diverge.
\begin{treat}
Scrivete una \lstinline{function img=julia(c)} che restituisca il frattale di Julia associato a $c$. La funzione:
\begin{itemize}
 \item genera 101 valori equispaziati nell'intervallo $[-2,2]$ con l'istruzione \lstinline{range=-2:0.04:2}.
 \item per ogni coppia $(i,j)$:
\begin{itemize}
\item calcola il punto $z_0$ del piano complesso \lstinline{z0=range(i)+1I*range(j)};
\item applica per 10 volte la funzione $f(z)=z^2+c$ a partire dal punto $z_0$
\item scrive in \lstinline{img(i,j)} l'\emph{arcotangente} del modulo del numero complesso $z_{10}$ così calcolato. Difatti i numeri hanno variazioni molto grosse (da 0 a $10^{300}$\dots), e disegnarli così come sono non produrrebbe un risultato interessante.
\end{itemize}
 \item restituisce la matrice \lstinline{img}
\end{itemize}
\end{treat}

\begin{figure}[h!bt]
 \begin{center}
\includegraphics[width=0.45\textwidth]{j1}
\includegraphics[width=0.45\textwidth]{j2}
\includegraphics[width=0.45\textwidth]{j3}
\includegraphics[width=0.45\textwidth]{j4}
 \end{center}
\caption{Alcuni frattali di Julia}
\end{figure}

\subsection{Versioni vettorizzate}
Il programma \lstinline{newton.m} è abbastanza lento; difatti non abbiamo dedicato alcuna attenzione all'ottimizzazione del numero di istruzioni eseguite (sono questioni tecniche e noiose --- non è lavoro per un matematico). Un primo passo per velocizzarlo è sostituire \lstinline{horner} e \lstinline{derivata} con le funzioni equivalenti che già esistono in Octave, \lstinline{polyval} e \lstinline{polyderiv} (controllare la sintassi con \lstinline{help}).

Un altro miglioramento potete provare a farlo da soli:
\begin{treat}
Utilizzando le funzioni \lstinline{polyval} e \lstinline{polyderiv} applicate con una matrice come secondo argomento (\lstinline{help polyval}) e le operazioni elemento-per-elemento (ad esempio \lstinline{.*}, \lstinline{./}), scrivete un'istruzione che esegua un passo del metodo di Newton \emph{contemporaneamente} su tutti i valori contenuti in una matrice $X$.
\end{treat}
Un po' più complicato infine è rendere efficiente la funzione \lstinline{decidi} per Newton. Riportiamo qui una versione più veloce delle funzioni che disegnano i frattali di Newton e Julia, che utilizza tutte queste ottimizzazioni. Se volete potete usarli per generare immagini più grandi o per zoomare su alcuni dettagli e studiare la forma dei due frattali.
\begin{lstlisting}
function img=disegna_newton(dimensione)
%restituisce un frattale di Newton nxn. Visualizzare con imagesc(img)
p=[1;0;0;1]; %coefficienti del polinomio
n=dimensione;
range=-2:4/(n-1):2; %griglia di punti

deg=length(p)-1;
dp=polyderiv(p);
radici=roots(p);

%matrice dei valori iniziali
X=ones(n,1)*range+range'*ones(1,n)*1I;

%esegue 10 passi di Newton "in parallelo", dovrebbero bastare
for k=1:10
  X=X-polyval(p,X)./polyval(dp,X);
endfor

%trova il punto piu' vicino "in parallelo" con un trucco:
%
%generiamo un "tensore", cioe' un oggetto a tre indici
%M(i,j,k)=X_{j,k}-radici(i)
%
%usiamo la funzione min: [values,positions]=min(A) calcola il minimo 
%lungo la prima dimensione (righe)
%e restituisce in positions le /posizioni/ in cui si trovano
%i  minimi su ogni riga
%ad es. [v p]=min([4.5 2.5 8 9]) restituisce v=2.5, p=2
%(perche' il minimo sta in posizione 2)
M=zeros(deg,n,n);
for i=1:deg
  M(i,:,:)=abs(X-radici(i));
endfor
[values positions]=min(M);

%ritrasforma positions da un "tensore" 1xnxn a una matrice nxn
img=reshape(positions,[n n]);
endfunction
\end{lstlisting}
\begin{lstlisting}
function img=julia(c)
%function img=julia(c)
%restituisce il disegno del frattale di Julia con parametro c
%plottare con imagesc(julia(c))
n=200;
range=-2:4/(n-1):2; %griglia di punti

%matrice dei valori iniziali
X=ones(n,1)*range+range'*ones(1,n)*1I;

for k=1:10
  X=X.*X+c;
endfor

img=atan(abs(X));
endfunction
\end{lstlisting}

\end{document}
