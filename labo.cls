\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{labo}[2010/10/24 Laboratorio di analisi numerica, (c) f.poloni@sns.it]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
\ProcessOptions\relax
\LoadClass[a4paper,10pt]{scrartcl}
\usepackage[italian]{babel}
%arguments: lesson number, date
\newcommand{\lezione}[2]{\title{Laboratorio di Analisi Numerica\\Lezione #1}\date{#2}}

\author{Federico Poloni \texttt{<\href{mailto:f.poloni@sns.it}{f.poloni@sns.it}>}}

\usepackage[usenames,dvipsnames]{color}
\usepackage{listings}

\usepackage{graphicx}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{url}

\usepackage[italian]{babel}
\usepackage{microtype}

\DeclareMathOperator{\range}{:}

\definecolor{MyLightGrey}{rgb}{0.95,0.95,0.95}
 \lstset{language=Octave,showstringspaces=false,morecomment=[l]\%,basicstyle=\small,backgroundcolor=\color{MyLightGrey},extendedchars=true,columns=fullflexible,basicstyle=\tt,commentstyle=\color{blue},keywordstyle=\color{Brown},morekeywords={transpose,polyderiv}} 

\theoremstyle{remark}
\newtheorem{esercizio}{Esercizio}
\newenvironment{treat}{\begin{esercizio}[facoltativo]}{\end{esercizio}}

\newcommand{\disclaimer}{%
\noindent\framebox{\begin{minipage}{\textwidth}\small\textbf{Quantit\`a di esercizi:} in questa dispensa ci sono \emph{pi\`u esercizi} di quanti uno studente medio riesce a farne durante una lezione di laboratorio, specialmente tenendo conto anche degli esercizi facoltativi. Questo \`e perch\'e sono pensate per ``tenere impegnati'' per tutta la lezione anche quegli studenti che gi\`a hanno un solido background di programmazione. \par Quindi fate gli esercizi che riuscite, partendo da quelli \emph{non} segnati come facoltativi, e non preoccupatevi se non li finite tutti! \end{minipage}}%
}

\usepackage{courier}
\usepackage[T1]{fontenc}
\usepackage{lmodern}