\documentclass{labo}

\usepackage[utf8]{inputenc}

\lezione{3}{10 Novembre 2010}

\begin{document}

\maketitle

\disclaimer

\section{Sottomatrici e determinanti}
Utilizzando l'operatore \lstinline{:}, in Octave è possibile selezionare un'intera sottomatrice di una matrice:
\begin{lstlisting}
octave:1> A=[1 2 3; 4 5 6; 7 8 9]
A =

  1  2  3
  4  5  6
  7  8  9

octave:2> A(2:3,2:3)
ans =

  5  6
  8  9

octave:3> A(2,:)
ans =

  4  5  6

octave:4> A(:,:)
ans =

  1  2  3
  4  5  6
  7  8  9
\end{lstlisting}
La sintassi \lstinline{a:b} seleziona tutte le righe/colonne comprese tra \lstinline{a} e \lstinline{b} (estremi inclusi); utilizzare semplicemente \lstinline{:} come indice di riga/colonna seleziona l'intera riga/colonna. Possiamo anche assegnare un valore a una sottomatrice selezionata in questo modo:
\begin{lstlisting}
octave:5> A(1:2,1:2)=eye(2)
A =

  1  0  3
  0  1  6
  7  8  9
\end{lstlisting}
Ovviamente le dimensioni devono essere compatibili: non posso selezionare una sottomatrice $2 \times 2$ e assegnarle il valore \lstinline{eye(3)}!

La seguente function ritorna la \emph{matrice minore} di $(i,j)$ in $A$, cioè la matrice che si ottiene eliminando la $i$-esima riga e la $j$-esima colonna di $A$.
\begin{center}
\includegraphics{minor}
\end{center}
\begin{lstlisting}
function B=minor(A,i,j)
  n=size(A)(1);
  X=A(1:i-1,1:j-1);
  Y=A(1:i-1,j+1:n);
  Z=A(i+1:n,1:j-1);
  W=A(i+1:n,j+1:n);
  B=[X Y; Z W];
endfunction
\end{lstlisting}
Abbiamo già visto che se \lstinline{X,Y,Z,W} sono numeri, la riga di codice \lstinline{B=[X Y; Z W]} crea la matrice
\[
 \begin{bmatrix}
  X & Y\\Z & W
 \end{bmatrix};
\]
ora vediamo che la stessa sintassi funziona anche se \lstinline{X,Y,Z,W} sono matrici di dimensioni ``compatibili'' e crea la matrice formata accostando i quattro blocchi.

\begin{esercizio}
Creare una \lstinline{function d=mydet(A)} che calcoli il determinante di una matrice quadrata $A$ utilizzando la formula di Laplace sulla prima riga, cioè
\[
 \det(A)=A_{11}\det A^{(11)}-A_{12} \det A^{(12)}+ A_{13} \det A^{(13)}- \dots + (-1)^{n+1} A_{1n} \det A^{(1n)}
\]
dove $A_{ij}$ è l'elemento di $A$ nella posizione $(i,j)$ e $A^{(ij)}$ è la matrice minore di $A$ rispetto a $(i,j)$. Hint: la funzione può essere \emph{ricorsiva}, cioè chiamare sé stessa al suo interno. Occhio a definire un caso base! 

Poi testarla su alcune matrici, confrontandola con la funzione \lstinline{det} di Octave, per esempio le matrici di Vandermonde \lstinline{V=vander(1:2)}, 
\lstinline{V=vander(1:3)}, \lstinline{V=vander(1:4)}\dots
\end{esercizio}

\section{Tempi di calcolo}
Le funzioni \lstinline{tic} e \lstinline{toc} misurano il tempo necessario ad eseguire più istruzioni. La prima fa partire il cronometro, la seconda lo ferma e restituisce il valore ottenuto. Per esempio, le istruzioni
\begin{lstlisting}
 tic;
  x=10;
  myexp2(x,500);
 t=toc;
\end{lstlisting}
salvano in \lstinline{t} il tempo necessario ad eseguire le due righe centrali.

La seguente funzione disegna un grafico del tempo impiegato per calcolare i determinanti delle matrici \lstinline{vander(1:n)} con \lstinline{n=1:7}.
\begin{lstlisting}
function plottimes();
  n=7;
  tempi=zeros(n,1); %prepara un vettore vuoto con i tempi
  for i=1:n
    A=vander(1:i); %la matrice viene generata prima del ``tic''
    tic;
    d=mydet(A);
    tempi(i)=toc;
  endfor
  plot(1:n,tempi);
endfunction
\end{lstlisting}
\begin{center}
 \includegraphics{mydettimes}
\end{center}

I tempi di calcolo crescono molto velocemente: difatti, con questo algoritmo, per calcolare un determinante di dimensione $n$ dobbiamo calcolarne $n$ di dimensione $n-1$; quindi vale
\[
 \operatorname{tempo}(n) \approx n\cdot \operatorname{tempo}(n-1)
\]
da cui $\operatorname{tempo}(n) \approx n! \cdot \operatorname{tempo}(1)$. Il nostro algoritmo quindi non è adatto a calcolare il determinante in modo efficiente.

\begin{treat}
Provare a misurare nello stesso modo i tempi della funzione \lstinline{det} di Octave. Purtroppo i risultati non sono altrettanto ben definiti in quanto la funzione è molto ottimizzata e i risultati dipendono da molti dettagli degli algoritmi e dell'architettura del calcolatore.
\end{treat}

\section{Determinante con l'eliminazione di Gauss}
\begin{esercizio}
Scrivete una \lstinline{function d=mydet2(A)} che calcoli il determinante utilizzando l'eliminazione di Gauss. Suggerimenti:
\begin{itemize}
 \item  utilizzate espressioni del tipo \lstinline{A(i,:)=A(i,:)+A(j,:)} per sommare due righe di una matrice, invece di scrivere un ciclo \lstinline{for}.
 \item per ora, ignorate il fatto che i pivot possono essere zero; se incontrate un pivot nullo, terminate con un messaggio di errore (potete farlo con l'istruzione \lstinline{error('ho incontrato un pivot nullo')}).
 \item per controllare che tutto vada bene, in una prima fase fatevi scrivere a schermo il valore di \lstinline{A} dopo ogni passo di eliminazione di Gauss
\end{itemize}
\end{esercizio}
\begin{treat}
Scrivete una funzione che scambi opportunamente le righe quando incontra un pivot nullo, in modo da poter continuare l'algoritmo. Occhio: cosa succede al determinante quando scambio due righe?
\end{treat}
Testate la funzione su alcune matrici, per esempio \lstinline{A=vander(1:10)}, confrontandola con \lstinline{det()} di Octave.
\begin{esercizio}
 Testate la funzione \lstinline{mydet2} su alcune matrici per cui dovrebbe generare un errore. Per esempio, generate usando \lstinline{rand} una matrice $10\times 10$ in cui tutte le entrate sono casuali, tranne la nona riga che è la somma delle otto righe precedenti.
\end{esercizio}
\begin{esercizio}
 Disegnare un grafico analogo a quello più sopra per i tempi di calcolo di \lstinline{mydet2}.
\end{esercizio}

\end{document}