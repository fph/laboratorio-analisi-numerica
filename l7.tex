\documentclass{labo}

% rubber: pdf

\usepackage[utf8]{inputenc}

\lezione{7}{12 gennaio 2011}

 \begin{document}

\maketitle

\disclaimer

\section{Jacobi e Gauss--Seidel per un problema differenziale in due dimensioni}
\subsection{Problema e modello}
Il nostro fisico di fiducia ci ha passato un problema in cui si chiede di calcolare che forma assume una membrana elastica (un ``foglio di gomma'', una ``tovaglia'') rettangolare appesa con tutto il suo bordo in posizione fissata, come nella figura qui sotto.
\begin{center}
 \includegraphics[width=.7\textwidth]{esempio}
\end{center}

Questo problema è una versione bidimensionale del problema della forma che assume una corda fissata agli estremi che abbiamo visto nella lezione 4. Possiamo discretizzare il problema immaginando che la superficie sia una griglia rettangolare fatta di $m \times n$ piccoli pesetti, ognuno caratterizzato da un peso e da un'altezza rispetto al suolo. I pesetti sul bordo, cioè quelli con coordinate $(1,j)$, $(m,j)$, $(i,1)$ oppure $(i,n)$, sono a un'altezza fissata. Su ognuno dei punti $(i,j)$ agiscono:
\begin{itemize}
 \item la forza peso data dal peso della ``tovaglia'' (o dai pesi appoggiati su di essa) nel punto $(i,j)$;
 \item le quattro tensioni che lo tengono attaccato ai punti $(i\pm 1,j)$ e $(i, j \pm 1)$.
\end{itemize}

\begin{figure}[!h]
 \includegraphics[width=\textwidth]{pesetti}
\caption{Le componenti verticali delle forze di tensione $F_{i,j\pm 1},F_{i\pm 1,j}$ sono proporzionali alla differenza di altezza tra i punti}
\end{figure}

Quindi in questo caso, le equazioni che rappresentano le altezze dei punti (coordinata $z$) sono
\begin{equation}\label{sys}
\begin{gathered}
 \kappa \cdot p_{i,j}=(z_{i+1,j}-z_{i,j})+(z_{i-1,j}-z_{i,j})+(z_{i,j+1}-z_{i,j})+(z_{i,j-1}-z_{i,j}),\\ \forall i=2,\dots,m-1,\,j=2,\dots,n-1.
\end{gathered}
\end{equation}
Notate che non ci sono equazioni che rappresentano l'altezza dei punti sul bordo: infatti questa è fissata dalle condizioni iniziali che forniamo!

Questo è un sistema lineare nelle altezze $z(i,j)$: scrivendo in forma estesa le \eqref{sys}, per esempio per $m=n=5$, otteniamo
\begin{equation}\label{pippo}
 \begin{bmatrix}
-4 & 1 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\
1 & -4 & 1 & 0 & 1 & 0 & 0 & 0 & 0\\
0 & 1 & -4 & 0 & 0 & 1 & 0 & 0 & 0\\
1 & 0 & 0 & -4 & 1 & 0 & 1 & 0 & 0\\
0 & 1 & 0 & 1 & -4 & 1 & 0 & 1 & 0\\
0 & 0 & 1 & 0 & 1 & -4 & 0 & 0 & 1\\
0 & 0 & 0 & 1 & 0 & 0 & -4 & 1 & 0\\
0 & 0 & 0 & 0 & 1 & 0 & 1 & -4 & 1\\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 1 & -4\\ 
 \end{bmatrix}
\begin{bmatrix}
z_{22}\\z_{23}\\z_{24}\\z_{32}\\z_{33}\\z_{34}\\z_{42}\\z_{43}\\z_{44}\\
\end{bmatrix}
=
\begin{bmatrix}
 \kappa p_{22}-z_{12}-z_{21}\\ \kappa p_{23}-z_{13}\\kp_{24}-z_{14}-z_{25}\\ \kappa p_{32}-z_{31}\\kp_{33}\\ \kappa p_{34}-z_{35}\\ \kappa p_{42}-z_{41}-z_{52}\\ \kappa p_{43}-z_{53}\\ \kappa p_{44}-z_{54}-z_{45}\\
\end{bmatrix}
\end{equation}
Il vettore soluzione di questo sistema lineare è quello che contiene gli elementi della matrice \lstinline{z(2:m-1,2:n-1)}, \emph{messi in colonna in un qualche ordine}.

\subsection{Gauss--Seidel senza scrivere esplicitamente la $\eqref{pippo}$}
Al crescere di $m$ e $n$, calcolare esplicitamente matrice e termine noto per fare i calcoli diventa complicato. Inoltre, la complessità che dobbiamo aspettarci da un solutore diretto come l'eliminazione di Gauss è $O((mn)^3)$, molto elevata anche per $m$ e $n$ moderati. Però possiamo scrivere le iterazioni che danno i metodi di Jacobi e Gauss--Seidel lavorando direttamente su $z$ ``come matrice'' e non ``come vettore'':
\begin{multline}\label{jgs}
 z_{i,j} \leftarrow -\frac{1}{4}\left[\kappa p_{i,j}-z_{i-1,j}-z_{i+1,j}-z_{i,j-1}-z_{i,j+1}       \right]\\ \quad \text{per $i=2,\dots,m-1$ e $j=2,\dots,n-1$}
\end{multline}
(dopo aver inizializzato correttamente le altezze dei punti del bordo).

\begin{esercizio}
Rendersi conto che tutte le istruzioni di update che bisogna eseguire in Jacobi e Gauss-Seidel per questo problema, implementati come nella lezione scorsa, sono nella forma \eqref{jgs}
\end{esercizio}

\begin{esercizio}
 Scrivere una funzione \lstinline{z=membrana(bordo, carico, r)} che prenda:
\begin{itemize}
 \item \lstinline{bordo}: matrice $m \times n$ tale che gli elementi sul suo ``bordo'' siano uguali alle altezze iniziali dei punti sul bordo del rettangolo (per esempio: altezza di tutti i punti sul bordo uguale a zero, \lstinline{zeros(m,n)})
\item \lstinline{carico}: matrice $m \times n$ tale che \lstinline{carico(i,j)} rappresenti il peso del punto $(i,j)$ (per esempio: peso di tutti i punti uguale a 1, \lstinline{ones(m,n)})
\end{itemize}
ed esegua $r$ iterazioni di Gauss--Seidel (o Jacobi), ritornando le altezze dei punti risultanti in una matrice $m \times n$ $z$.
Potete porre $\kappa=1$, tanto è solo una questione di scala usata per misurare distanze e forze.
\end{esercizio}
Una volta calcolata la soluzione, possiamo disegnare su schermo il risultato come nelle immagini qui sopra con la funzione \lstinline{mesh}:
\begin{lstlisting}
octave:24> m=n=40;
octave:25> bordo=zeros(m,n);
octave:26> carico=ones(m,n);
octave:27> z=membrana(bordo, carico, 50);
octave:28> mesh(1:n,1:m,z)
\end{lstlisting}
\begin{center}
 \includegraphics[width=.7\textwidth]{esempio2}
\end{center}
\begin{esercizio}
Provare a vedere cosa succede se:
\begin{itemize}
 \item cambiamo \lstinline{carico}: per esempio, ``appoggiamo'' un peso sulla tovaglia, o imponiamo che la metà di destra della tovaglia pesi $1$ e la metà di sinistra pesi $2$.
 \item cambiamo \lstinline{bordo}: per esempio, il bordo di sinistra ad altezza $1$, il bordo di destra ad altezza $n$, e l'$i$-esimo elemento del bordo in alto e in basso ad altezza $i$ per ``raccordare'' in modo continuo gli altri due bordi. (cosa succede invece se le altezze sul bordo sono discontinue?)
 \item appoggiamo un peso in un punto specifico della ``tovaglia''
\end{itemize}
\end{esercizio}

\section{Esercizi facoltativi}
\begin{treat}
 Chi converge più velocemente, Jacobi o Gauss--Seidel? Se avete visto il teorema di Stein--Rosenberg a lezione, dovreste essere in grado di prevederlo teoricamente\dots
\end{treat}

\begin{treat}
Come si potrebbe risolvere lo stesso problema per una superficie non rettangolare, senza fare troppe modifiche (e in particolare senza lavorare esplicitamente con la matrice).
\end{treat}

\subsection{Metodi multigrid}
Gauss--Seidel converge più velocemente se scegliamo come vettore iniziale una buona stima della soluzione. Come si può ottenere una buona stima della soluzione di un problema differenziale? Partendo dalla soluzione di un sistema più piccolo ottenuto partendo da una griglia a maglie più larghe! Per esempio, nel disegno qui sotto, invece di lavorare sulla griglia nera (+ bordo rosso), possiamo lavorare sulla griglia più larga formata dai soli nove punti blu.
\begin{figure}[h!t]
 \begin{center}
  \includegraphics[width=0.4\textwidth]{multigrid}
 \end{center}
\end{figure}
\begin{treat}
Supponete $m=n=2^{k+1}+1$, e scrivete una funzione che:
\begin{itemize}
 \item \emph{restringe} i dati iniziali sui pesi e le altezze al bordo alla griglia più larga $(2^k+1) \times (2^k+1)$ (come?), riducendo così il problema a uno di dimensione minore.
 \item risolve il sistema più piccolo $(2^k+1) \times (2^k+1)$ con il metodo visto sopra
 \item \emph{estende} la soluzione alla griglia più grande $(2^{k+1}+1) \times (2^{k+1}+1)$ approssimando in qualche modo i valori delle altezze nei punti mancanti (come?)
 \item usa questa soluzione approssimata come punto di partenza per il metodo di Gauss--Seidel.
\end{itemize}
Potete anche applicare questo approccio ricorsivamente\dots
\end{treat}
\end{document}